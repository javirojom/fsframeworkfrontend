import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { EvolutionChartCardComponent } from './evolution-chart-card.component';
import { UploadFileModule } from 'app/upload-file/upload-file.module';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    EvolutionChartCardComponent
  ],
  exports: [
    EvolutionChartCardComponent
  ]
})
export class EvolutionChartCardModule { }
