import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvolutionChartCardComponent } from './evolution-chart-card.component';

describe('EvolutionChartCardComponent', () => {
  let component: EvolutionChartCardComponent;
  let fixture: ComponentFixture<EvolutionChartCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvolutionChartCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvolutionChartCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
