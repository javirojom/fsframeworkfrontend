import { Component, OnInit, Input, OnChanges, SimpleChanges} from "@angular/core";
import * as Chartist from "chartist";
import { from } from "rxjs";
import { startWith } from "rxjs-compat/operator/startWith";

export enum EVOLUTION_CHART_TYPES {
  line = 1,
  barchart = 2,
}

export enum EVOLUTION_CHART_TENDENCY {
  positive = 1,
  negative = 2,
}


@Component({
  selector: "evolution-chart-card",
  templateUrl: "./evolution-chart-card.component.html",
  styleUrls: ["./evolution-chart-card.component.css"],
})
export class EvolutionChartCardComponent implements OnInit, OnChanges {

  public color: string = "warning";
  public tendency: Array<number> = [];

  private _name: any;
  private _description: any;
  private _values: any;
  private _type: any;
  private _objective: any;

  @Input()
  public identifier:string;
  
  @Input()
  set objective(val: any) {
    this._objective = val;
  }

  @Input()
  set description(val: any) {
    this._description = val;
  }
  @Input()
  set name(val: any) {
    this._name = val;
  }
  @Input()
  set values(val: any) {
    this._values = val;
    this.computeTendencies();
  }
  @Input()
  set type(val: any) {
    this._type = val;
  }

  get objective(): any {
    return this._objective;
  }

  get description(): any {
    return this._description;
  }
  get name(): any {
    return this._name;
  }
  get values(): any {
    return this._values;
  }

  get type(): any {
    return this._type;
  }

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
      this.updateChart();
  }

  computeTendencies(){
    this.tendency = [];
    let bad_tendencies = [];
    //TODO poner variable para contemplar decrementos como positivos
    this.values.forEach(serie => {
      let first=serie[0];
      let last=serie[serie.length-1];
      if (last<first){
        bad_tendencies.push(true);
      }
      else{
        if(last==first){
          let value = this.objective==EVOLUTION_CHART_TENDENCY.positive ? false : true;
          bad_tendencies.push(value); 
        }
        else{
          bad_tendencies.push(false);
        }
      }
      this.tendency.push((last/first)*100)
    });
    
    if((!bad_tendencies.includes(true) && this.objective==EVOLUTION_CHART_TENDENCY.positive) || (!bad_tendencies.includes(false) && this.objective==EVOLUTION_CHART_TENDENCY.negative)){
      this.color="success";
    } else{
      if((!bad_tendencies.includes(false) && this.objective==EVOLUTION_CHART_TENDENCY.positive) || (!bad_tendencies.includes(true) && this.objective==EVOLUTION_CHART_TENDENCY.negative)){
        this.color="danger";
      }
      else{
        this.color="warning";
      }
    }
  }

  updateChart() {

    console.log(this.values);

    const data: any = {
      labels: [],
      series: undefined,
    };

    data.series = this.values;
    for (let i = 0; i < this.getLongestSeries(this.values); i++) {
      data.labels.push(i);
    }

    switch (this.type) {
      case EVOLUTION_CHART_TYPES.line:
        this.generateLineChart(data);
        break;

      case EVOLUTION_CHART_TYPES.barchart:
        this.generateBarChart(data);
        break;

      default:
        console.log("Type of chart " + this.type + " not found");
        break;
    }
  }

  getLongestSeries(series) {
    let max_length = 0;
    series.forEach((serie) => {
      let length = serie.length;
      if (length > max_length) {
        max_length = length;
      }
    });
    return max_length;
  }


  getColorTendency(value){
    if (this.objective==EVOLUTION_CHART_TENDENCY.positive){
      if(value>=100){
        return "success";
      }
      if(value<50){
        return "danger";
      }
      return "warning";
    }
    else{
      if(value<=100){
        return "success";
      }
      if(value>150){
        return "danger";
      }
      return "warning";
    }
  }

  getArrowTendency(value){
    if(value>100){
      return "up";
    }
    if(value==100){
      return "right";
    }
    return "down";
  }

  getLowestValue(series){
    let lowest_value = undefined;

    series.forEach(serie => {
      serie.forEach(value => {
        if(value<lowest_value || lowest_value == undefined){
          lowest_value = value;
        }
      });
    });
    lowest_value = lowest_value==undefined ? 0 : lowest_value;

    return lowest_value;
  }

  getUpperValue(series){
    let uper_value = undefined;

    series.forEach(serie => {
      serie.forEach(value => {
        if(value>uper_value || uper_value == undefined){
          uper_value = value;
        }
      });
    });
    uper_value = uper_value==undefined ? 100 : uper_value;

    return uper_value;
  }

  /*-------------- GENERATE CHARTS */

  generateLineChart(data) {

    let low_val = this.getLowestValue(data.series);
    let uper_val = this.getUpperValue(data.series);

    if ((uper_val-low_val) <50){
      uper_val = uper_val*10;
    }
    low_val = low_val <0 ? Math.floor(low_val) : 0;
    uper_val = Math.ceil(uper_val + ((uper_val*2)/10 - (uper_val*2)%10*0.1));

    console.log("Low val"+low_val);
    console.log("uper val"+uper_val);

    const options: any = {
      lineSmooth: Chartist.Interpolation.cardinal({
        tension: 0,
      }),
      low: low_val,
      high: uper_val,
      chartPadding: { top: 0, right: 0, bottom: 0, left: 0 },
    };

    var chart = new Chartist.Line("#myChartId-"+this.identifier, data, options);

    this.startAnimationForLineChart(chart);
  }

  generateBarChart(data) {

    let low_val = this.getLowestValue(data.series);
    let uper_val = this.getUpperValue(data.series);

    if ((uper_val-low_val) <50){
      uper_val = uper_val*10;
    }
    low_val = low_val <0 ? Math.floor(low_val) : 0;
    uper_val = Math.ceil(uper_val + ((uper_val*2)/10 - (uper_val*2)%10*0.1));

    console.log("Low val"+low_val);
    console.log("uper val"+uper_val);

    var options = {
      axisX: {
        showGrid: false,
      },
      low: low_val,
      high: uper_val,
      chartPadding: { top: 0, right: 5, bottom: 0, left: 0 },
    };
    var responsiveOptions: any[] = [
      [
        "screen and (max-width: 640px)",
        {
          seriesBarDistance: 5,
          axisX: {
            labelInterpolationFnc: function (value) {
              return value[0];
            },
          },
        },
      ],
    ];
    var websiteViewsChart = new Chartist.Bar(
      "#myChartId-"+this.identifier,
      data,
      options,
      responsiveOptions
    );
  }
  /*-------------- ANIMATIONS FOR CHARTS */

  startAnimationForLineChart(chart) {
    let seq: any, delays: any, durations: any;
    seq = 0;
    delays = 80;
    durations = 500;

    chart.on("draw", function (data) {
      if (data.type === "line" || data.type === "area") {
        data.element.animate({
          d: {
            begin: 600,
            dur: 700,
            from: data.path
              .clone()
              .scale(1, 0)
              .translate(0, data.chartRect.height())
              .stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint,
          },
        });
      } else if (data.type === "point") {
        seq++;
        data.element.animate({
          opacity: {
            begin: seq * delays,
            dur: durations,
            from: 0,
            to: 1,
            easing: "ease",
          },
        });
      }
    });

    seq = 0;
  }
  startAnimationForBarChart(chart) {
    let seq2: any, delays2: any, durations2: any;

    seq2 = 0;
    delays2 = 80;
    durations2 = 500;
    chart.on("draw", function (data) {
      if (data.type === "bar") {
        seq2++;
        data.element.animate({
          opacity: {
            begin: seq2 * delays2,
            dur: durations2,
            from: 0,
            to: 1,
            easing: "ease",
          },
        });
      }
    });

    seq2 = 0;
  }
}
