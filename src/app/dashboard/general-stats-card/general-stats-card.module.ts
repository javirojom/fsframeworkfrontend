import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { GeneralStatsCardComponent } from './general-stats-card.component';
import { UploadFileModule } from 'app/upload-file/upload-file.module';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    GeneralStatsCardComponent
  ],
  exports: [
    GeneralStatsCardComponent
  ]
})
export class GeneralStatsCardModule { }
