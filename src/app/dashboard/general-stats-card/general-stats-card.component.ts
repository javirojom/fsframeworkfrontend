import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "general-stats-card",
  templateUrl: "./general-stats-card.component.html",
  styleUrls: ["./general-stats-card.component.css"],
})
export class GeneralStatsCardComponent implements OnInit {
  
    private _color: any;
  private _icon: any;
    private _name: any;
  private _value: any;
  private _stats: any;

  @Input()
  set color(val: any) { this._color = val; }

  @Input()
  set icon(val: any) { this._icon = val; }
  @Input()
  set name(val: any) { this._name = val; }
  @Input()
  set value(val: any) { this._value = val; }
  @Input()
  set stats(val: any) { this._stats = val; }

  get color(): any { return this._color; }
  get icon(): any { return this._icon; }
  get name(): any { return this._name; }
  get value(): any { return this._value; }
  get stats(): any { return this._stats; }

  constructor() {}

  ngOnInit(): void {}
}
