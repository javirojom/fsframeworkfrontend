import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureFunctionCardComponent } from './feature-function-card.component';

describe('FeatureFunctionCardComponent', () => {
  let component: FeatureFunctionCardComponent;
  let fixture: ComponentFixture<FeatureFunctionCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureFunctionCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureFunctionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
