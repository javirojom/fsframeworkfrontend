import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

export enum FEATURE_USAGE { input = 1, output = 2};

@Component({
  selector: "feature-function-card",
  templateUrl: "./feature-function-card.component.html",
  styleUrls: ["./feature-function-card.component.css"],
})
export class FeatureFunctionCardComponent implements OnInit {

  FEATURE_USAGE = FEATURE_USAGE;

  private _features: any;
  @Input()
  set features(val: any) {
    this._features = val;
  }
  get features(): any {
    return this._features;
  }

  private _disable: any;
  @Input()
  set disable(val: any) {
    this._disable = val;
  }
  get disable(): any {
    return this._disable;
  }

  @Output()
  public featuresFunctionsChangesEmit = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}


  notifyChangeInFeatures() {
    this.featuresFunctionsChangesEmit.emit(this.features);
  }

  changeToInput(index){
    this.features[index].usage=FEATURE_USAGE.input;
    this.notifyChangeInFeatures();
  }

  
  changeToOutput(index){
    this.features[index].usage=FEATURE_USAGE.output;
    this.notifyChangeInFeatures();
  }


  isInput(usage){
    return usage==FEATURE_USAGE.input;
  }


  isOutput(usage){
    return usage==FEATURE_USAGE.output;
  }

  



}
