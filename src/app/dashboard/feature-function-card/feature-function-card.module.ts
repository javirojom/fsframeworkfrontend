import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FeatureFunctionCardComponent } from './feature-function-card.component';
import { UploadFileModule } from 'app/upload-file/upload-file.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    FeatureFunctionCardComponent
  ],
  exports: [
    FeatureFunctionCardComponent
  ]
})
export class FeatureFunctionCardModule { }
