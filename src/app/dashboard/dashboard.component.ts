import { Component, OnInit } from "@angular/core";
import { BackendService } from "app/backend.service";
import * as Chartist from "chartist";
import {
  EVOLUTION_CHART_TENDENCY,
  EVOLUTION_CHART_TYPES,
} from "./evolution-chart-card/evolution-chart-card.component";
import { FEATURES_TYPES } from "./selection-card/selection-card.component";
import { FEATURE_USAGE } from "./feature-function-card/feature-function-card.component";
import {saveAs as importedSaveAs} from "file-saver";

declare var $: any;

export enum TYPE_OF_PROBLEM {
  classification = "classification",
  regression = "regression",
}
@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent implements OnInit {
  /*FLAGS FOR KNOW ACTUAL STEP */
  //dataset uploaded not need flag. It is known by token undefined or not
  public isInitialSelection: Boolean = false;
  public outliersFiltered: Boolean = false;
  public executing: Boolean = false;

  /*STEP 0: upload dataset */
  public token: String = undefined;

  /*STEP 1: select initial features and filter outliers (optional)*/
  public features_stats: any = undefined;

  /*STEP 2: select features based on set from Feature Selection techniques*/
  public best_r2_score_sets: any = undefined;
  public minimum_features_sets: any = undefined;
  public most_frequent_sets: any = undefined;

  /*STEP 1, 2 AND 3 */
  public features: any = undefined;


  /*COMMON STEPS: SHOWING INFORMATION IN CARDS*/

  /*dictionaries to format upper stats */
  samples_stats: any = null;
  n_features_stats: any = null;
  n_selected_stats: any = null;
  best_model_stats: any = null;

  /*arrays for evolution stats */
  evolutions_features_stats: any = null;
  evolutions_r2score_stats: any = null;
  evolutions_mse_stats: any = null;

  evolution_number_features_selected: Array<number> = [];
  evolution_r2_score: Array<number> = [];
  evolution_mse: Array<number> = [];
  evolution_mae: Array<number> = [];

  /*general information for stats*/
  number_of_outliers: number = undefined;
  number_of_original_samples: number = undefined;
  number_of_using_samples: number = undefined;

  number_of_original_features: number = undefined;
  number_of_original_inputs: number = undefined;
  number_of_original_outputs: number = undefined;

  number_of_selected_features: number = undefined;
  number_of_selected_inputs: number = undefined;
  number_of_selected_outputs: number = undefined;

  best_model_r2_score: number = undefined;
  best_model_mse: number = undefined;
  best_model_mae: number = undefined;

  constructor(public _bckend: BackendService) {
    this.updateGeneralStats();
  }

  updateEvolutionStats() {
    this.evolutions_features_stats = {
      name: "Number of features",
      descriptions: ["Number of features selected"],
      values: [this.evolution_number_features_selected],
      type: EVOLUTION_CHART_TYPES.line,
      objective: EVOLUTION_CHART_TENDENCY.negative,
    };

    this.evolutions_r2score_stats = {
      name: "R2 score",
      descriptions: ["R2 score"],
      values: [this.evolution_r2_score],
      type: EVOLUTION_CHART_TYPES.line,
      objective: EVOLUTION_CHART_TENDENCY.positive,
    };

    this.evolutions_mse_stats = {
      name: "MSE and MAE",
      descriptions: ["MSE", "MAE"],
      values: [this.evolution_mse, this.evolution_mae],
      type: EVOLUTION_CHART_TYPES.line,
      objective: EVOLUTION_CHART_TENDENCY.positive,
    };
  }

  updateGeneralStats() {
    this.samples_stats = {
      color: "info",
      icon: "content_copy",
      name: "Number of samples",
      value:
        this.number_of_original_samples == undefined
          ? "---"
          : this.number_of_original_samples,
      stats: [
        {
          icon: "info_outline",
          name: "Number of outliers",
          value:
            this.number_of_outliers == undefined
              ? "---"
              : this.number_of_outliers,
        },
        {
          icon: "info_outline",
          name: "Using",
          value:
            this.number_of_using_samples == undefined
              ? "---"
              : this.number_of_using_samples,
        },
      ],
    };

    this.n_features_stats = {
      color: "info",
      icon: "store",
      name: "Number of original features",
      value:
        this.number_of_original_features == undefined
          ? "---"
          : this.number_of_original_features,
      stats: [
        {
          icon: "info_outline",
          name: "Number of inputs",
          value:
            this.number_of_original_inputs == undefined
              ? "---"
              : this.number_of_original_inputs,
        },
        {
          icon: "info_outline",
          name: "Number of outputs",
          value:
            this.number_of_original_outputs == undefined
              ? "---"
              : this.number_of_original_outputs,
        },
      ],
    };

    this.n_selected_stats = {
      color: "success",
      icon: "info_outline",
      name: "Number of selected features",
      value:
        this.number_of_selected_features == undefined
          ? "---"
          : this.number_of_selected_features,
      stats: [
        {
          icon: "info_outline",
          name: "Number of inputs",
          value:
            this.number_of_selected_inputs == undefined
              ? "---"
              : this.number_of_selected_inputs,
        },
        {
          icon: "info_outline",
          name: "Number of outputs",
          value:
            this.number_of_selected_outputs == undefined
              ? "---"
              : this.number_of_selected_outputs,
        },
      ],
    };

    this.best_model_stats = {
      color: "warning",
      icon: "info_outline",
      name: "Best model score",
      value:
        this.best_model_r2_score == undefined
          ? "---"
          : this.best_model_r2_score,
      stats: [
        {
          icon: "info_outline",
          name: "R2 score",
          value:
            this.best_model_r2_score == undefined
              ? "---"
              : this.best_model_r2_score,
        },
        {
          icon: "info_outline",
          name: "MSE",
          value: this.best_model_mse == undefined ? "---" : this.best_model_mse,
        },
        {
          icon: "info_outline",
          name: "MAE",
          value: this.best_model_mae == undefined ? "---" : this.best_model_mae,
        },
      ],
    };

    console.log(this.samples_stats);
  }

  //// FIRST STEP: LOAD DATASET

  onDatasetLoaded(token: String): void {
    console.log("token received");
    console.log(token);
    if (token === null) {
      this.showErrorMessage("The dataset has not uploaded properly. Retry!");
    } else {
      this.token = token;
      localStorage.setItem("FS_Framework_token", token.toString());
      this.showSuccesfulMessage("Dataset has been loaded properly");
      this.loadDescriptionOfDataset();
    }
  }

  loadDescriptionOfDataset() {
    this._bckend.loadDescription(this.token).subscribe(
      (res) => {
        localStorage.setItem(
          "FS_Framework_dataset_description",
          JSON.stringify(res)
        );
        this.loadDescriptionOfDatasetIntoVariables(res);
        this.updateGeneralStats();
      },
      (error) => {
        this.showErrorMessage("ERROR. Reload to restore session");
      }
    );
  }

  loadDescriptionOfDatasetIntoVariables(description) {
    console.log(description);
    this.features = this.generateFeatureList(
      Object.keys(description.data_types),
      description.data_types,
      true
    );
    this.features_stats = description.statistics;
    this.number_of_original_features = description.number_of_features;
    this.number_of_original_inputs = this.getNumberInputs();
    this.number_of_original_outputs = this.getNumberOutputs();
    this.number_of_original_features = description.number_of_features;
    this.number_of_original_samples = description.number_of_samples;
    this.number_of_using_samples = description.number_of_samples;
  }

  executeOutliersDetection() {
    this.executing = true;
    this._bckend
      .executeOutliersDetection(
        this.token,
        this.getNameOfSelectedFeatures(),
        this.getTypeOfSelectedFeatures()
      )
      .subscribe(
        (res) => {
          this.executing = false;
          this.number_of_original_samples = res.number_of_original_rows;
          this.number_of_outliers =
            res.number_of_original_rows - res.number_of_filtered_rows;
          this.number_of_using_samples = res.number_of_filtered_rows;
          this.outliersFiltered = true;
          this.updateGeneralStats();
          this.showSuccesfulMessage("Outliers detection done properly!");
        },
        (error) => {
          this.executing = false;
          if (error.status == 404) {
            this.showWarningMessage(
              "There are not any sample without empty values on one or more of the selected features"
            );
            this.showInfoMessage(
              "Outliers detection not executed. Change the features"
            );
          } else {
            this.showErrorMessage("ERROR. Reload to restore session");
          }
        }
      );
  }

  onFeaturesSelectedChanges(features) {
    console.log(this.features);

    if (this.isInitialSelection == false) {
      this.number_of_original_inputs = this.getNumberInputs();
      this.number_of_original_outputs = this.getNumberOutputs();
    } else {
      this.number_of_selected_inputs = this.getNumberInputs();
      this.number_of_selected_outputs = this.getNumberOutputs();
    }
    this.updateGeneralStats();
  }

  onFeaturesFunctionsChanges(features) {
    console.log(this.features);

    if (this.isInitialSelection == false) {
      this.number_of_original_inputs = this.getNumberInputs();
      this.number_of_original_outputs = this.getNumberOutputs();
    } else {
      this.number_of_selected_inputs = this.getNumberInputs();
      this.number_of_selected_outputs = this.getNumberOutputs();
    }
    this.updateGeneralStats();
  }

  executeSelection() {
    let inputs = this.getNameOfSelectedInputs();
    let outputs = this.getNameOfSelectedOutputs();
    let types_of_features = this.getTypeOfSelectedFeatures();
    let type = this.determineTypeOfProblem(outputs, types_of_features);

    if (outputs.length == 0) {
      this.showWarningMessage("There must be at least one output");
    } else {
      if (inputs.length <= 1) {
        this.showWarningMessage("There must be at least two inputs to select");
      } else {
        this.executing = true;
        this._bckend
          .executeSelection(
            this.token,
            inputs,
            outputs,
            types_of_features,
            type
          )
          .subscribe(
            (res) => {
              this.executing = false;
              this.isInitialSelection = true;
              this.putValuesOfSelectionIntoArraysOfSets(res);
              this.putValuesOfSelectionIntoStatsVariables(inputs, outputs, res);
              this.updateGeneralStats();
              this.updateEvolutionStats();
              this.showSuccesfulMessage("Selection done properly!");
            },
            (error) => {
              this.executing = false;
              if (error.status == 404) {
                this.showWarningMessage(
                  "There are not any sample without empty values on one or more of the selected features"
                );
                this.showInfoMessage(
                  "Feature Selection not executed. Change the features"
                );
              } else {
                this.showErrorMessage("ERROR. Reload to restore session");
              }
            }
          );
      }
    }
  }


  putValuesOfSelectionIntoArraysOfSets(res){
    this.best_r2_score_sets = res["best_r2_score_sets"];
    this.minimum_features_sets = res["minimum_features_sets"];
    this.most_frequent_sets = res["most_frequent_sets"];
  }

  putValuesOfSelectionIntoStatsVariables(inputs, outputs, res) {
    this.number_of_selected_features = inputs.length + outputs.length;
    this.number_of_selected_inputs = inputs.length;
    this.number_of_selected_outputs = outputs.length;
    this.best_model_r2_score = res["initial_score"];
    this.best_model_mse = res["initial_mse"];
    this.best_model_mae = res["initial_mae"];

    this.evolution_number_features_selected.push(this.number_of_selected_features);
    this.evolution_r2_score.push(this.best_model_r2_score);
    this.evolution_mse.push(this.best_model_mse);
    this.evolution_mae.push(this.best_model_mae);
  }

  downloadFile(data) {
    const blob = new Blob([data]);
    importedSaveAs(blob, "model.zip");
  }

  generateModel() {
    let inputs = this.getNameOfSelectedInputs();
    let outputs = this.getNameOfSelectedOutputs();
    let types_of_features = this.getTypeOfSelectedFeatures();
    let type = this.determineTypeOfProblem(outputs, types_of_features);

    if (outputs.length == 0) {
      this.showWarningMessage("There must be at least one output");
    } else {
      if (inputs.length == 0) {
        this.showWarningMessage("There must be at least one input");
      } else {
        this.executing = true;
        this._bckend
          .downloadModel(this.token, inputs, outputs, types_of_features, type)
          .subscribe(
            (data) => {
              this.executing = false;
              this.downloadFile(data);
              this.showSuccesfulMessage("Model downloaded properly!");
            },
            (error) => {
              this.executing = false;
              console.log(error);
              if (error.status == 404) {
                this.showWarningMessage(
                  "There are not any sample without empty values on one or more of the selected features"
                );
                this.showInfoMessage(
                  "Model generation not executed. Change the features"
                );
              } else {
                this.showErrorMessage("ERROR. Reload to restore session");
              }
            }
          );
      }
    }
  }

  determineTypeOfProblem(outputs, types_of_features) {
    outputs.forEach((o_name) => {
      if (types_of_features[o_name] == FEATURES_TYPES.categoric) {
        return TYPE_OF_PROBLEM.classification;
      }
    });

    return TYPE_OF_PROBLEM.regression;
  }

  getNumberInputs() {
    let number = 0;
    this.features.forEach((f) => {
      if (f.selected == true && f.usage == FEATURE_USAGE.input) {
        number = number + 1;
      }
    });

    return number;
  }

  getNumberOutputs() {
    let number = 0;
    this.features.forEach((f) => {
      if (f.selected == true && f.usage == FEATURE_USAGE.output) {
        number = number + 1;
      }
    });

    return number;
  }

  getFeatureTypeFromDescription(type) {
    let discrete_types = ["Object", "object", "string", "String"];

    if (discrete_types.includes(type)) {
      return FEATURES_TYPES.categoric;
    } else {
      return FEATURES_TYPES.cuantitative;
    }
  }

  generateFeatureList(features_names, features_info, selected) {
    var features = [];
    features_names.forEach((f) => {
      features.push({
        name: f,
        type: this.getFeatureTypeFromDescription(features_info[f]),
        selected: selected,
        usage: FEATURE_USAGE.input,
      });
    });
    return features;
  }

  getNameOfSelectedFeatures() {
    let selected_features = [];
    this.features.forEach((f) => {
      if (f["selected"] == true) {
        selected_features.push(f.name);
      }
    });
    return selected_features;
  }

  getNameOfSelectedInputs() {
    let selected_features = [];
    this.features.forEach((f) => {
      if (f["selected"] == true && f["usage"] == FEATURE_USAGE.input) {
        selected_features.push(f.name);
      }
    });
    return selected_features;
  }

  getNameOfSelectedOutputs() {
    let selected_features = [];
    this.features.forEach((f) => {
      if (f["selected"] == true && f["usage"] == FEATURE_USAGE.output) {
        selected_features.push(f.name);
      }
    });
    return selected_features;
  }

  getTypeOfSelectedFeatures() {
    let types = {};
    this.features.forEach((f) => {
      if (f["selected"] == true) {
        types[f["name"]] = f["type"];
      }
    });
    return types;
  }

  showInfoMessage(message) {
    this.showNotification("info", "bottom", "right", message);
  }

  showErrorMessage(message) {
    this.showNotification("danger", "bottom", "right", message);
  }

  showSuccesfulMessage(message) {
    this.showNotification("success", "bottom", "right", message);
  }

  showWarningMessage(message) {
    this.showNotification("warning", "bottom", "right", message);
  }

  showNotification(_type, from, align, _message) {
    console.log("Showing message: " + _message);

    //const type = ['','info','success','warning','danger'];

    //const color = Math.floor((Math.random() * 4) + 1);

    $.notify(
      {
        icon: "notifications",
        message: _message,
      },
      {
        //type: type[color],
        type: _type,
        timer: 4000,
        placement: {
          from: from,
          align: align,
        },
        template:
          '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
          '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          "</div>" +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
          "</div>",
      }
    );
  }

  ngOnInit() {
    var token = localStorage.getItem("FS_Framework_token");
    console.log(token);
    if (token !== null) {
      // STEP 1 COMPLETED. DATASET UPLOADED
      if (window.confirm("Continue with previous dataset?")) {
        this.token = token;
        var initialDescription = localStorage.getItem(
          "FS_Framework_dataset_description"
        );
        if (initialDescription == null) {
          this.loadDescriptionOfDataset();
        } else {
          const initialDescriptionJSON = JSON.parse(initialDescription);
          this.loadDescriptionOfDatasetIntoVariables(initialDescriptionJSON);
          this.updateGeneralStats();
        }
        var outliersFiltered = Boolean(
          localStorage.getItem("FS_Framework_outliers_filtered_done") == "true"
        );
        if (outliersFiltered) {
          this.outliersFiltered = outliersFiltered;
          this.number_of_outliers = Number(
            localStorage.getItem("FS_Framework_number_of_outliers")
          );
          this.number_of_original_samples = Number(
            localStorage.getItem("FS_Framework_number_of_original_samples")
          );
          this.number_of_outliers = Number(
            localStorage.getItem("FS_Framework_number_of_original_samples")
          );
        }

        var isInitialSelection = Boolean(
          localStorage.getItem("FS_Framework_initial_selection_done") == "true"
        );
        if (isInitialSelection) {
          // STEP 2 COMPLETED. DATASET UPLOADED
          //TODO traerse el resto de datos y realizar acciones de acuerdo a su estado
        }

        this.showSuccesfulMessage("Data from previous session loaded");
      }
    }

    /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */

    const dataDailySalesChart: any = {
      labels: ["M", "T", "W", "T", "F", "S", "S"],
      series: [[12, 17, 7, 17, 23, 18, 38]],
    };

    const optionsDailySalesChart: any = {
      lineSmooth: Chartist.Interpolation.cardinal({
        tension: 0,
      }),
      low: 0,
      high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
      chartPadding: { top: 0, right: 0, bottom: 0, left: 0 },
    };

    var dailySalesChart = new Chartist.Line(
      "#dailySalesChart",
      dataDailySalesChart,
      optionsDailySalesChart
    );

    this.startAnimationForLineChart(dailySalesChart);

    /* ----------==========     Completed Tasks Chart initialization    ==========---------- */

    const dataCompletedTasksChart: any = {
      labels: ["12p", "3p", "6p", "9p", "12p", "3a", "6a", "9a"],
      series: [[230, 750, 450, 300, 280, 240, 200, 190]],
    };

    const optionsCompletedTasksChart: any = {
      lineSmooth: Chartist.Interpolation.cardinal({
        tension: 0,
      }),
      low: 0,
      high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
      chartPadding: { top: 0, right: 0, bottom: 0, left: 0 },
    };

    var completedTasksChart = new Chartist.Line(
      "#completedTasksChart",
      dataCompletedTasksChart,
      optionsCompletedTasksChart
    );

    // start animation for the Completed Tasks Chart - Line Chart
    this.startAnimationForLineChart(completedTasksChart);

    /* ----------==========     Emails Subscription Chart initialization    ==========---------- */

    var datawebsiteViewsChart = {
      labels: ["J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"],
      series: [[542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]],
    };
    var optionswebsiteViewsChart = {
      axisX: {
        showGrid: false,
      },
      low: 0,
      high: 1000,
      chartPadding: { top: 0, right: 5, bottom: 0, left: 0 },
    };
    var responsiveOptions: any[] = [
      [
        "screen and (max-width: 640px)",
        {
          seriesBarDistance: 5,
          axisX: {
            labelInterpolationFnc: function (value) {
              return value[0];
            },
          },
        },
      ],
    ];
    var websiteViewsChart = new Chartist.Bar(
      "#websiteViewsChart",
      datawebsiteViewsChart,
      optionswebsiteViewsChart,
      responsiveOptions
    );

    //start animation for the Emails Subscription Chart
    this.startAnimationForBarChart(websiteViewsChart);
  }

  /*-------------- ANIMATIONS FOR CHARTS */

  startAnimationForLineChart(chart) {
    let seq: any, delays: any, durations: any;
    seq = 0;
    delays = 80;
    durations = 500;

    chart.on("draw", function (data) {
      if (data.type === "line" || data.type === "area") {
        data.element.animate({
          d: {
            begin: 600,
            dur: 700,
            from: data.path
              .clone()
              .scale(1, 0)
              .translate(0, data.chartRect.height())
              .stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint,
          },
        });
      } else if (data.type === "point") {
        seq++;
        data.element.animate({
          opacity: {
            begin: seq * delays,
            dur: durations,
            from: 0,
            to: 1,
            easing: "ease",
          },
        });
      }
    });

    seq = 0;
  }
  startAnimationForBarChart(chart) {
    let seq2: any, delays2: any, durations2: any;

    seq2 = 0;
    delays2 = 80;
    durations2 = 500;
    chart.on("draw", function (data) {
      if (data.type === "bar") {
        seq2++;
        data.element.animate({
          opacity: {
            begin: seq2 * delays2,
            dur: durations2,
            from: 0,
            to: 1,
            easing: "ease",
          },
        });
      }
    });

    seq2 = 0;
  }
}
