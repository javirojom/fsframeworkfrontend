import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "description-table",
  templateUrl: "./description-table.component.html",
  styleUrls: ["./description-table.component.css"],
})
export class DescriptionTableComponent implements OnInit {
  private _stats: any;
  @Input()
  set stats(val: any) {
    console.log("Reading stats...")
    this._stats = val;
    console.log(val);
    this.updateRowsAndColumns();
    console.log(this.rows);
    console.log(this.columns);
  }
  get stats(): any {
    return this._stats;
  }


  public rows = undefined;
  public columns = undefined;

  constructor() {}

  ngOnInit(): void {}

  updateRowsAndColumns(){
    this.rows = Object.keys(this.stats);
    let one_row = this.rows[0];
    if (one_row!=null){
      this.columns = Object.keys(this.stats[one_row]);
    }
  }


  getFormattedValue(value){
    return value==null ? "---" : String(value).slice(0,10);
  }
}
