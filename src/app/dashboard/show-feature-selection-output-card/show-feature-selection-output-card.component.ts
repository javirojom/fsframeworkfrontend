import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "show-feature-selection-output-card",
  templateUrl: "./show-feature-selection-output-card.component.html",
  styleUrls: ["./show-feature-selection-output-card.component.css"],
})
export class ShowFeatureSelectionOutputCardComponent implements OnInit {
  private _sets: any;
  @Input()
  set sets(val: any) {
    this._sets = val;
    this.see_all = new Array(this.sets.length).fill(false);
    console.log(this.sets);
  }
  get sets(): any {
    return this._sets;
  }

  public see_all : Array<boolean> = undefined;

  private _name_of_sets: any;

  @Input()
  set name_of_sets(val: any) {
    this._name_of_sets = val;
  }
  get name_of_sets(): any {
    return this._name_of_sets;
  }

  @Input()
  public icons

  ngOnInit(): void {
      
  }

  getIdByIndex(i){
    return "PanelFS"+this.name_of_sets[i].replaceAll(/\s/g,'');
  }
}
