import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowFeatureSelectionOutputCardComponent } from './show-feature-selection-output-card.component';

describe('ShowFeatureSelectionOutputCardComponent', () => {
  let component: ShowFeatureSelectionOutputCardComponent;
  let fixture: ComponentFixture<ShowFeatureSelectionOutputCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowFeatureSelectionOutputCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowFeatureSelectionOutputCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
