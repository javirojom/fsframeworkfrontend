import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ShowFeatureSelectionOutputCardComponent } from './show-feature-selection-output-card.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    ShowFeatureSelectionOutputCardComponent
  ],
  exports: [
    ShowFeatureSelectionOutputCardComponent
  ]
})
export class ShowFeatureSelectionOutputCardModule { }
