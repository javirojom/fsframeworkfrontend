import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { UploadFileModule } from 'app/upload-file/upload-file.module';
import { GeneralStatsCardModule } from './general-stats-card/general-stats-card.module';
import { EvolutionChartCardModule } from './evolution-chart-card/evolution-chart-card.module';
import { SelectionCardModule } from './selection-card/selection-card.module';
import { FeatureFunctionCardModule } from './feature-function-card/feature-function-card.module';
import { DescriptionTableModule } from './description-table/description-table.module';
import { ShowFeatureSelectionOutputCardModule } from './show-feature-selection-output-card/show-feature-selection-output-card.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    UploadFileModule,
    GeneralStatsCardModule,
    EvolutionChartCardModule,
    SelectionCardModule,
    FeatureFunctionCardModule,
    DescriptionTableModule,
    ShowFeatureSelectionOutputCardModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    DashboardComponent,
  ]
})
export class DashboardModule { }
