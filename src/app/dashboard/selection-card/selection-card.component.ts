import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FEATURE_USAGE } from "../feature-function-card/feature-function-card.component";

export enum FEATURES_TYPES {
  categoric = "C",
  cuantitative = "N",
}

@Component({
  selector: "selection-card",
  templateUrl: "./selection-card.component.html",
  styleUrls: ["./selection-card.component.css"],
})
export class SelectionCardComponent implements OnInit {

  FEATURE_USAGE = FEATURE_USAGE;

  private _features: any;
  @Input()
  set features(val: any) {
    this._features = val;
  }
  get features(): any {
    return this._features;
  }

  @Output()
  public featuresSelectedChangesEmit = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  getNameofType(type) {
    if (type == FEATURES_TYPES.categoric) {
      return "Discrete";
    } else {
      return "Continuous";
    }
  }

  searchFeatureIndex(features, name) {
    let index = null;
    for (let i = 0; i < features.length; i++) {
      if (features[i].name == name) {
        index = i;
      }
    }
    return index;
  }

  notifyChangeInFeatures() {
    this.featuresSelectedChangesEmit.emit(this.features);
  }

  changeTypeOfFeature(index) {
    //let index = this.searchFeatureIndex(this.features, name);

    if (index != null) {
      if (this.features[index].type == FEATURES_TYPES.categoric) {
        this.features[index].type = FEATURES_TYPES.cuantitative;
      } else {
        this.features[index].type = FEATURES_TYPES.categoric;
      }
    }

    this.notifyChangeInFeatures();
  }

  unselectAllFeatures() {
    if (confirm("Are you sure of unselect all features?")) {
      this.changeFeaturesSelection(false);
      this.notifyChangeInFeatures();
    }
  }

  selectAllFeatures() {
    if (confirm("Are you sure of select all features?")) {
      this.changeFeaturesSelection(true);
      this.notifyChangeInFeatures();
    }
  }

  changeFeaturesSelection(selected) {
    for (let i = 0; i < this.features.length; i++) {
      this.features[i].selected = selected;
    }
  }
}
