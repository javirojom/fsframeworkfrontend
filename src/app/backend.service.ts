import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import 'rxjs/Rx' ;

@Injectable({
  providedIn: "root",
})
export class BackendService {

  BASE_URL : String = "http://localhost:33366";

  constructor(private _http: HttpClient) {}

  uploadFile(formData) : Observable<any> {
    return this._http.post(this.BASE_URL+"/file", formData, {
      reportProgress: true,
      observe: 'events'
  });
  }


  loadDescription(token): Observable<any> {
    const httpOptions = { headers: new HttpHeaders({'Access-Control-Allow-Headers': 'Content-Type', 'Content-Type': 'application/json' }) };
    return this._http.get(this.BASE_URL+"/description/"+token, httpOptions);
  }


  executeOutliersDetection(token, inputs, types): Observable<any> {
    let formData = { "token": token, "inputs": inputs, "types": types};
    console.log(formData);
    const httpOptions = { headers: new HttpHeaders({'Access-Control-Allow-Headers': 'Content-Type', 'Content-Type': 'application/json' }) };
    return this._http.post(this.BASE_URL+"/outliers", formData, httpOptions);
  }

  executeSelection(token, inputs, outputs, types, type): Observable<any> {
    let formData = { "token": token, "inputs": inputs, "outputs": outputs, "types": types, "type":type};
    console.log(formData);
    const httpOptions = { headers: new HttpHeaders({'Access-Control-Allow-Headers': 'Content-Type', 'Content-Type': 'application/json' }) };
    return this._http.post(this.BASE_URL+"/select", formData, httpOptions);
  }



  downloadModel(token, inputs, outputs, types, type): Observable<any> {
    let formData = { "token": token, "inputs": inputs, "outputs": outputs, "types": types, "type":type};
    const httpOptions = { headers: new HttpHeaders({'Accept': '*/*', 'Accept-Encoding':'gzip, deflate, br'}), responseType: 'blob' as const};
    return this._http.post(this.BASE_URL+"/model", formData, httpOptions);
  }

}
