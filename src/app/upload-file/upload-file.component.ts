import { Component, EventEmitter, OnInit, Output } from "@angular/core";

import { BackendService } from "app/backend.service";

import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpEventType } from "@angular/common/http";
import { Subscription } from "rxjs";
import { finalize
 } from "rxjs/operators";
@Component({
  selector: "app-upload-file",
  templateUrl: "./upload-file.component.html",
  styleUrls: ["./upload-file.component.css"],
})
export class UploadFileComponent {
  @Output()
  fileUploadedEvent = new EventEmitter<string>();

  requiredFileType:string = "csv";

  fileName = '';
  uploadProgress:number = 0;
  uploadSub: Subscription;

  constructor(private bckend: BackendService) {}


  onFileSelected(event) {
    const file:File = event.target.files[0];
  
    if (file) {
        this.fileName = file.name;
        const formData = new FormData();
        formData.append("file", file);

        const upload$ = this.bckend.uploadFile(formData).pipe(
            finalize(() => this.reset())
        );
      
        this.uploadSub = upload$.subscribe( event => {
          if(event.ok == false){
            console.log("Not uploaded properly.");
            this.emitToken(null);
          }
          else{
            console.log(event);
            if (event.type == HttpEventType.UploadProgress) {
              this.uploadProgress = Math.round(100 * (event.loaded / event.total));
              console.log(this.uploadProgress);
            } else{
              if(event.type == HttpEventType.Response){
                console.log(event);
                const token = event.body["token"];
                console.log(token);
                this.emitToken(token);
                console.log("Uploaded Successfully.");
          }}}
        }, (error) => {
          console.log("Not uploaded properly.");
          this.emitToken(null);
        })
    }
}

cancelUpload() {
this.uploadSub.unsubscribe();
this.reset();
}

reset() {
this.uploadProgress = null;
this.uploadSub = null;
this.fileName = '';
}

  emitToken(token) {
    this.fileUploadedEvent.emit(token);
  }
}
